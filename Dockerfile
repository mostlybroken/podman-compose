# syntax=docker/dockerfile:1

FROM quay.io/podman/stable:latest

RUN dnf -y update \
    && dnf -y install podman-compose \
    && rm -rf /var/cache/dnf/* \
    && rm -rf /var/cache/yum/*
